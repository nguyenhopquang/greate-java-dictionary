/**
 * Class Word chua thuoc tinh word_target va word_explain
 */
public class Word {
    //Khoi tao thuoc tinh word_target, word_explain cho class

    private String word_target;
    private String word_explain;

    /**
     * Getter/Setter
     */
    public String getWord_target() {
        return word_target;
    }
    public void setWord_target(String word_target) {
        this.word_target = word_target;
    }

    public String getWord_explain() {
        return word_explain;
    }

    public void setWord_explain(String word_explain) {
        this.word_explain = word_explain;
    }

    public Word(String english,String vietnam){
        this.word_target=english;
        this.word_explain=vietnam;
    }
}
