import java.util.Scanner;
import java.io.*;
import java.util.*;

public class DictionaryManagement {

    public static void dictionaryLookup() {
        String filename="data.txt";
        String line = null;
        ArrayList<String> wordList = new ArrayList<String>();
        try {
            FileReader data = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(data);
            int i=1;
            while((line = bufferedReader.readLine()) != null) {
                wordList.add(line);
            }
            bufferedReader.close();
            System.out.print("Nhập từ cần tìm :");
            Scanner scanIn = new Scanner(System.in);
            String keyword = scanIn.nextLine();
            for(String each : wordList){
                String[] wordsArray;
                wordsArray= each.split("\t");
                if (wordsArray[0].equals(keyword)) {
                    System.out.println("Nghĩa của " +wordsArray[0]+" là : "+wordsArray[1]);
                    break;
                }
            }
        }
        catch(FileNotFoundException ex) {
            System.out.println("Khong the mo file '" + filename + "'");
        }
        catch(IOException ex) {
            System.out.println("Khong the doc file'" + filename + "'");
        }
    }

    public static void insertToData(Word word) {
        String filename="data.txt";
        try {
            FileWriter fileWriter = new FileWriter(filename, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            String temp= word.getWord_target() +"\t"+word.getWord_explain();
            bufferedWriter.write(temp);
            bufferedWriter.newLine();
            bufferedWriter.close();
        }
        catch(IOException ex) {
            System.out.println("Lỗi khi ghi file '" + filename + "'");
        }
    }

    public static void insertFromCommandline() {
        Scanner scanIn = new Scanner(System.in);
        int soTu;
        String english, vietnam;
        System.out.print("Nhập số từ cần thêm vào từ điển:");
        soTu = scanIn.nextInt();
        scanIn.nextLine();
        for (int i=0;i<soTu;i++) {
            System.out.print("Nhập từ tiếng anh thứ "+(i+1)+ ": ");
            english = scanIn.nextLine();
            System.out.print("Nhập nghĩa cho từ thứ "+(i+1)+": ");
            vietnam = scanIn.nextLine();
            Word newword= new Word(english,vietnam);
            insertToData(newword);
            System.out.println("Đã nhập xong dữ liệu");
        }
    }
    public static void insertFromFile() {
        String infile ="dictionaries.txt";
        String outfile = "data.txt";

        String line = null;
        try {
            FileReader readfile = new FileReader(infile);
            BufferedReader readfileBuffer = new BufferedReader(readfile);
            String[] wordsArray;
            while((line = readfileBuffer.readLine()) != null) {
                    wordsArray = line.split("\t");
                    Word newword= new Word(wordsArray[0], wordsArray[1]);
                    insertToData(newword);
                    System.out.println("...");
            }
            System.out.println("Đã nhập xong dữ liệu !");

            readfileBuffer.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Khong the mo file '" + infile + "'");
        }
        catch(IOException ex) {
            System.out.println("Khong the doc file'" + infile + "'");
        }
    }
    public static void main(String[] args) {
        dictionaryLookup();
    }

}
