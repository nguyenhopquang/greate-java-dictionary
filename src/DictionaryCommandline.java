import java.util.Scanner;
import java.io.*;
public class DictionaryCommandline {
    public static void dictionaryAdvanced() {
        Scanner scanIn = new Scanner(System.in);
        System.out.println("Nhập lựa chọn của bạn:");
        System.out.println("1. Nhập dữ liệu từ file");
        System.out.println("2. Hiển thị tất cả các từ");
        System.out.println("3. Tìm kiếm từ điển");
        int choice= scanIn.nextInt();
        if (choice==1) {
            DictionaryManagement dm = new DictionaryManagement();
            dm.insertFromFile();
        } else if (choice==2) {
           showAllWords();
        } else if (choice==3) {
            DictionaryManagement dm = new DictionaryManagement();
            dm.dictionaryLookup();
        }
    }
    public static void showAllWords() {
        String filename= "data.txt";
        String line = null;
        try {
            FileReader data = new FileReader(filename);
            BufferedReader bufferedReader = new BufferedReader(data);
            int i=1;
            while((line = bufferedReader.readLine()) != null) {
                System.out.println(i+"|"+line);
                i++;
            }
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println("Khong the mo file '" + filename + "'");
        }
        catch(IOException ex) {
            System.out.println("Khong the doc file'" + filename + "'");
        }
    }
    public static void dictionaryBasic() {
        Scanner scanIn = new Scanner(System.in);
        System.out.println("Nhập lựa chọn của bạn:");
        System.out.println("1. Hiển thị tất cả các từ trong từ điển");
        System.out.println("2. Nhập dữ liệu từ bàn phím");
        System.out.println("3. Nhập dữ liệu từ file dictionaries.txt");
        System.out.println("4. Tìm kiếm từ điển");
        int choice= scanIn.nextInt();
        if (choice==1) {
            showAllWords();
        } else if (choice==2) {
            DictionaryManagement dm= new DictionaryManagement();
            dm.insertFromCommandline();
        } else if (choice==3) {
            DictionaryManagement dm = new DictionaryManagement();
            dm.insertFromFile();
        } else if (choice==4) {
            DictionaryManagement dm = new DictionaryManagement();
            dm.dictionaryLookup();
        }
    }
    public static void main(String[] args) {
        dictionaryBasic();
    }
}
